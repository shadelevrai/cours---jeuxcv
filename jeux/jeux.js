var deplacementADroiteOuGauche; // variable du deplacement à gauche et à droite
var deplacementEnHautOuBas; // variable du deplacement en haut et en bas
var eclair = 0; // variable pour l'éclair
var directionCollision = 0;
var tableau = [];
var nuage = 0;
var nuageNombre = 0;


$(function () {






    //******************************************************************************************//
    //********************************Décor*****************************************************//
    //*******************************************************************************************//

    $('body').css({
        backgroundImage: 'url(blue.jpg)',
    })
    $('body').prepend('<div class="fourreTout"></div>');

    $('.fourreTout').prepend('<div class="espaceJeu"></div>')
    $('.espaceJeu').css({
        backgroundImage: 'url(fond.png)',
        marginLeft: '20px',
        marginTop: '5px',
        width: '1020px',
        height: '700px',
        position: 'absolute',
        zIndex: '0',
        overflow: 'hidden',
        borderStyle: 'double',
        borderColor: 'white',
    })
    $('.espaceJeu').prepend('<div class="fondNoir"></div>');
    $('.fondNoir').css({
        backgroundColor: 'black',
        top: '0px',
        width: '1430px',
        height: '850px',
        position: 'absolute',
        zIndex: '2',
    });

    //*******************************************************************************************//
    //****************************************Nuage**********************************************//
    //********************************************************************************************//


    $('.espaceJeu').prepend('<div class="fog"></div>');
    setInterval(function () {
        if (nuageNombre == 0) {
            $('.fog').css({
                backgroundImage: 'url(fog7.png)',
                width: '1530px',
                height: '850px',
                zIndex: '3',
                position: 'absolute',
                opacity: '0.2',
                top: '0px',
                left: nuage - 100 + 'px',
            });
            nuage += 1;
        }
        if (nuageNombre == 1) {
            $('.fog').css({
                backgroundImage: 'url(fog7.png)',
                width: '1530px',
                height: '850px',
                zIndex: '3',
                position: 'absolute',
                opacity: '0.2',
                top: '0px',
                left: nuage - 100 + 'px',
            });
            nuage -= 1;
        }

        if (nuage == 100) {
            nuageNombre = 1;
        };
        if (nuage == 0) {
            nuageNombre = 0;
        };
    }, 100)

    //********************************************************************************************//
    //******************************************Son***********************************************//
    //*******************************************************************************************//



    $('body').prepend('<audio id="sonLose"src="lose.mp3"></audio>');
    $('body').prepend('<audio id="sonCompetenceHtml"src="coincoin.mp3"></audio>');
    $('body').prepend('<audio id="sonCompetenceCss"src="coincoin.mp3"></audio>');
    $('body').prepend('<audio id="sonCompetenceJavascript"src="coin.mp3"></audio>');
    $('body').prepend('<audio id="sonCompetenceJquery"src="coincoin.mp3"></audio>');
    $('body').prepend('<audio id="sonCompetenceBootstrap"src="coincoin.mp3"></audio>');
    $('body').prepend('<audio id="sonCompetenceAngular"src="coincoin.mp3"></audio>');
    $('body').prepend('<audio id="sonCompetencePhotoshop"src="coincoin.mp3"></audio>');
    $('body').prepend('<audio id="sonCompetenceIndesign"src="coincoin.mp3"></audio>');
    $('body').prepend('<audio id="sonCompetenceNode"src="coincoin.mp3"></audio>');
    $('body').prepend('<audio id="sonCompetenceAjax"src="coincoin.mp3"></audio>');
    $('body').prepend('<audio id="sonCompetenceMongo"src="coincoin.mp3"></audio>');
    $('body').prepend('<audio id="sonCompetenceMeteor"src="coincoin.mp3"></audio>');
    $('body').prepend('<audio id="sonWin"src="win.mp3"></audio>');
    $('body').prepend('<audio id="ambiance"src="all.mp3" autoplay></audio>');


    //******************************************************************************************//
    //***************************************JOUEUR*********************************************//
    //******************************************************************************************//


    $('.espaceJeu').prepend('<div class="perso"></div>');
    $('.perso').css({
        width: '22px',
        height: '31px',
        top: '650px',
        left: '510px',
        position: 'absolute',
        backgroundImage: 'url(sprite/persohaut.png)',
        zIndex: '4',
    });


    //**********************************************************************************************//
    //************************** BARRE DE VIE DU JOUEUR*********************************************//
    //*********************************************************************************************//


    var barreDeVie = 100;
    $('.espaceJeu').prepend('<div class="progressBar"></div>');
    setInterval(function () {

        $('.progressBar').css({
            position: 'absolute',
            top: '50px',
            left: '30px',
            width: '200px',
            height: '31px',
            backgroundColor: 'lightgreen',
            zIndex: '3',
            borderRadius: '10px',
            borderStyle: 'solid',
            borderWidth: '1px',
            borderColor: 'darkgreen',

        });

        if (barreDeVie < 1) {
            $('.progressBar').text('Je crois que les filles aux cheveux longs t\'aiment un peu trop...');
        }
    }, 100);

    $('.progressBar').prepend('<div class="progressBar2"></div>');
    $('.progressBar2').css({
        height: '30px',
        backgroundColor: 'green',
        borderRadius: '10px',
        borderStyle: 'solid',
        borderWidth: '1px',
        borderColor: 'darkgreen',
    });
    $('.progressBar').prepend('<div class="notePv"></div>');
    setInterval(function () {
        $('.progressBar2').css({
            width: barreDeVie + '%',
        });
        $('.notePv').css({
            left: '10px',
            padding: '4px',
            color: 'black',
            position: 'absolute',
        }).text('Vos points de vie : ' + barreDeVie + ' Pv');
    }, 100);



    //**************************************************************************************//
    //**************************************eclair******************************************//
    //**************************************************************************************//


    $('.espaceJeu').prepend('<div class="progressEclair"></div>');

    setInterval(function () {
        $('.progressEclair').css({
            position: 'absolute',
            top: '50px',
            left: '770px',
            width: '200px',
            height: '31px',
            backgroundColor: 'lightgrey',
            zIndex: '3',
            borderRadius: '10px',
            borderStyle: 'solid',
            borderWidth: '1px',
            borderColor: 'darkred',
        })
    }, 100);

    $('.progressEclair').prepend('<div class="eclairBarre"></div>');
    $('.eclairBarre').css({
        backgroundColor: 'red',
        height: '30px',
        width: eclair + 'px',
        borderRadius: '10px',
        borderStyle: 'solid',
        borderWidth: '1px',
        borderColor: 'darkred',
    })
    setInterval(function () {
        if (eclair == 100) {
            $('.eclairBarre').css({
                width: '200px',
            });
        } else {
            $('.eclairBarre').css({
                width: eclair + '%',
            });
        }
    }, 100);

    setInterval(function () {
        if (eclair < 100) {
            eclair += 2;
        }
    }, 200);

    $('.espaceJeu').prepend('<div class="noteEclair"></div>');
    setInterval(function () {
        $('.noteEclair').css({
            left: '790px',
            top: '50px',
            padding: '4px',
            color: 'black',
            position: 'absolute',
            zIndex: '4',
        }).text('Votre compétence : ' + eclair + ' %');

        if (eclair == 100) {
            $('.noteEclair').css({
                left: '810px'
            }).text('Touche espace !');
        }
    }, 100);

    $('body').keydown(function (event) {

        switch (event.which) {
        case 32:
            if (eclair == 100) {
                $('body').prepend('<audio id="storm"src="storm.mp3" autoplay></audio>');
                $('.imgHtml2').hide();
                $('.imgCss2').hide();
                $('.imgJs2').hide();
                $('.imgJquery2').hide();
                $('.imgBootstrap2').hide();
                $('.imgAngular2').hide();
                $('.imgPhotoshop2').hide();
                $('.imgIndesign2').hide();
                $('.imgNodejs2').hide();
                $('.imgAjax2').hide();
                $('.imgMongo2').hide();
                $('.imgMeteor2').hide();
                $('.lampehaut').hide();
                $('.lampebas').hide();
                $('.lampegauche').hide();
                $('.lampedroite').hide();
                $('.bordBas').hide();
                $('.bordHaut').hide();
                $('.bordGauche').hide();
                $('.bordDroite').hide();
                $('.fondBlanc').remove();
                $('.espaceJeu').prepend('<div class="fondBlanc"></div>');
                $('.fondBlanc').css({
                    width: '1430px',
                    height: '850px',
                    backgroundColor: 'white',
                })
                $('.fondBlanc').hide(0);
                $('.fondBlanc').fadeIn(70);
                $('.fondBlanc').fadeOut(930);
                setTimeout(function () {
                    $('.espaceJeu').prepend('<div class="fondNoirEclair"></div>');
                    $('.fondNoirEclair').css({
                        backgroundColor: 'black',
                        width: '1430px',
                        height: '850px',
                        position: 'absolute',
                        zIndex: '2',
                    });
                    $('.fondNoirEclair').hide();
                    $('.fondNoirEclair').fadeIn(1000);
                }, 1000);
                setTimeout(function () {
                    $('.imgHtml2').show();
                    $('.imgCss2').show();
                    $('.imgJs2').show();
                    $('.imgJquery2').show();
                    $('.imgBootstrap2').show();
                    $('.imgAngular2').show();
                    $('.imgPhotoshop2').show();
                    $('.imgIndesign2').show();
                    $('.imgNodejs2').show();
                    $('.imgAjax2').show();
                    $('.imgMongo2').show();
                    $('.imgMeteor2').show();
                    $('.lampehaut').show();
                    $('.lampebas').show();
                    $('.lampegauche').show();
                    $('.lampedroite').show();
                    $('.bordBas').show();
                    $('.bordHaut').show();
                    $('.bordGauche').show();
                    $('.bordDroite').show();
                    $('.fondNoirEclair').remove();
                }, 2000);
                eclair = 0;
            }
            break;
        };
    });



    //****************************************************************************************//
    //*******************************Si le joueur perd****************************************//
    //***************************************************************************************//


    $('.perdu').prepend('<divclass="perduTitre"></div>');

    setInterval(function fonctionPerd() {
        if (barreDeVie == 0) {
            barreDeVie = 100000000;
            $('.espaceJeu').remove();
            $('.perdu').remove();
            $('.menuHaut').remove();
            $('.fourreTout').prepend('<div class="perdu"></div>');
            $('.perdu').css({
                position: "absolute",
                top: "50px",
                left: '50px',
                width: '1020px',
                height: '0px',
                backgroundImage: 'url(blue.jpg)',

                borderStyle: 'double',
                borderColor: 'white',
            }).animate({
                height: '700px',
            }, 800);
            $('.sonAmbiant').remove();
            document.getElementById('sonLose').play();


            $('.perdu').prepend('<div class="perduTitre"></div>');
            $('.perduTitre').css({
                width: '50%',
                height: '100px',
                marginLeft: '25%',
                color: 'white',
                textAlign: 'center',
                fontFamily: 'segoe UI',
                fontSize: '100px',
            }).text('PERDU !');

            $('.perdu').prepend('<div class="perduTexte"></div>');
            $('.perduTexte').css({
                width: '80%',
                height: '200px',
                marginTop: '200px',
                marginLeft: '10%',
                color: 'white',
                textAlign: 'center',
                fontFamily: 'segoe UI',
                fontSize: '50px',
                position: 'absolute',
            }).text('Mais ne vous découragez pas ! Le rhum ne s\'est pas fait en un jour.')

            $('.perdu').prepend('<div class="perduRecom"></div>');
            $('.perduRecom').css({
                width: '90%',
                height: '100px',
                marginTop: '400px',
                marginLeft: '5%',
                color: 'white',
                textAlign: 'center',
                fontFamily: 'segoe UI',
                fontSize: '40px',
                position: 'absolute',
                padding: '20px',
                cursor: 'pointer',
            }).text('Pour recommencer, cliquer ici');

            $(".perduRecom").click(function () {
                window.open('http://djoher.com/jeux', '_self', false);
            });

            $('.perdu').prepend('<div class="perduCv"></div>');
            $('.perduCv').css({
                width: '90%',
                height: '100px',
                marginTop: '500px',
                marginLeft: '5%',
                color: 'white',
                textAlign: 'center',
                fontFamily: 'segoe UI',
                fontSize: '36px',
                position: 'absolute',
                padding: '24px',
                cursor: 'pointer',
            }).text('Vous en avez marre ? Cliquez ici pour mon CV en PDF');

            $(".perduCv").click(function () {
                window.open('http://djoher.com/Cv/CV-Jeu-Djoher.pdf');
            });


        }
    }, 100);



    //*************************************************************************************//
    //****************************déplacement du joueur************************************//
    //*************************************************************************************//



    deplacementADroiteOuGauche = 510;
    deplacementEnHautOuBas = 650;
    $('body').keydown(function (event) {

        switch (event.which) {
        case 39: // va à droite
            if (eclair > 20) {
                $('.fondNoir').remove();
                if (deplacementADroiteOuGauche < 1000) {
                    deplacementADroiteOuGauche += 4;
                    directionCollision = 0;
                    $('.perso').css({
                        left: deplacementADroiteOuGauche + 'px',
                        backgroundImage: 'url(sprite/persogauche.png)',
                        animation: 'playpersogauche 0.5s steps(3) infinite',
                    });
                    $('.lampedroite').remove();
                    $('.lampebas').remove();
                    $('.lampehaut').remove();
                    $('.lampegauche').remove();
                    $('.espaceJeu').prepend('<img class="lampedroite" src="lampedroite2.png" />');
                    $('.lampedroite').css({
                        position: 'absolute',
                        top: deplacementEnHautOuBas - 40 + 'px',
                        left: deplacementADroiteOuGauche + 20 + 'px',
                        zIndex: '3',
                    })
                };
            };
            break;

        case 37: // va à gauche
            if (eclair > 20) {
                $('.fondNoir').remove();
                if (deplacementADroiteOuGauche > 1) {
                    deplacementADroiteOuGauche -= 4;
                    directionCollision = 1;
                    $('.perso').css({
                        left: deplacementADroiteOuGauche + 'px',
                        backgroundImage: 'url(sprite/persodroite.png)',
                        animation: 'playpersodroite 0.5s steps(3) infinite',
                    });
                    $('.lampedroite').remove();
                    $('.lampebas').remove();
                    $('.lampehaut').remove();
                    $('.lampegauche').remove();
                    $('.espaceJeu').prepend('<img class="lampegauche" src="lampegauche2.png" />');
                    $('.lampegauche').css({
                        position: 'absolute',
                        top: deplacementEnHautOuBas - 40 + 'px',
                        left: deplacementADroiteOuGauche - 160 + 'px',
                        zIndex: '3',
                    })
                };
            };
            break;

        case 40: // va en bas
            if (eclair > 20) {
                $('.fondNoir').remove();
                if (deplacementEnHautOuBas < 670) {
                    deplacementEnHautOuBas += 4;
                    directionCollision = 2;
                    $('.perso').css({
                        top: deplacementEnHautOuBas + 'px',
                        backgroundImage: 'url(sprite/persobas.png)',
                        animation: 'playpersobas 0.5s steps(3) infinite',
                    })

                    $('.lampehaut').remove();
                    $('.lampebas').remove();
                    $('.lampegauche').remove();
                    $('.lampedroite').remove();

                    $('.espaceJeu').prepend('<img class="lampebas" src="lampebas2.png" />')
                    $('.lampebas').css({
                        position: 'absolute',
                        top: deplacementEnHautOuBas + 28 + 'px',
                        left: deplacementADroiteOuGauche - 50 + 'px',
                        zIndex: '3',
                    })
                };
            };
            break;

        case 38: // va en haut
            if (eclair > 20) {


                $('.fondNoir').remove();
                if (deplacementEnHautOuBas > 100) {
                    deplacementEnHautOuBas -= 4;
                    directionCollision = 3;
                    $('.perso').css({
                        top: deplacementEnHautOuBas + 'px',
                        backgroundImage: 'url(sprite/persohaut.png)',
                        animation: 'playpersohaut 0.5s steps(3) infinite',
                    });
                    $('.lampehaut').remove();
                    $('.lampebas').remove();
                    $('.lampegauche').remove();
                    $('.lampedroite').remove();

                    $('.espaceJeu').prepend('<img class="lampehaut" src="lampehaut2.png" />');
                    $('.lampehaut').css({
                        position: 'absolute',
                        top: deplacementEnHautOuBas - 150 + 'px',
                        left: deplacementADroiteOuGauche - 50 + 'px',
                        zIndex: '3',
                    })
                };
                break;
            };
        };
    });


    //**********************************************************************************************//
    //**********************fond noir évolutif*****************************************************//
    //***********************************************************************************************//


    $('.espaceJeu').prepend('<div class ="bordBas"></div>'); // la div qui part du bas et qui augmente quand on monte
    $('.espaceJeu').prepend('<div class ="bordHaut"></div>'); // la div qui part du haut et qui diminue quand on monte
    $('.espaceJeu').prepend('<div class="bordGauche"></div>'); // la div qui part de gauche et qui dominue quand on va à gauche
    $('.espaceJeu').prepend('<div class="bordDroite"></div>'); // la div qui part de droite et qui augmente quand on va à gauche
    $('.bordBas').css({
        backgroundColor: 'black',
        position: 'absolute',
        zIndex: '3',
    });
    $('.bordHaut').css({
        backgroundColor: 'black',
        position: 'absolute',
        zIndex: '3',
    });
    $('.bordDroite').css({
        backgroundColor: 'black',
        position: 'absolute',
        zIndex: '3',
    });
    $('.bordGauche').css({
        backgroundColor: 'black',
        position: 'absolute',
        zIndex: '3',
    });
    $('body').keydown(function (event) {

        switch (event.which) {
        case 39: // va à droite
            console.log(deplacementADroiteOuGauche);
            if (deplacementADroiteOuGauche < 1430) {

                $('.bordDroite').css({
                    width: '1600px',
                    height: '1050px',
                    top: '0px',
                    left: deplacementADroiteOuGauche + 200 + 'px',
                })
                $('.bordGauche').css({

                    width: deplacementADroiteOuGauche + 20 + 'px',
                    height: '1050px',
                    top: '0px',
                    left: '0px',
                })
                $('.bordHaut').css({

                    width: '1430px',
                    height: deplacementEnHautOuBas - 34 + 'px',
                    top: '0px',
                    left: '0px',
                })
                $('.bordBas').css({

                    width: '1430px', // largeur de la div
                    height: '850px', // aerty est une variable qui commence à 54. Elle baissera de 4 à chaque fois que la touche bas est appuyée
                    top: deplacementEnHautOuBas + 72 + 'px', // la div aura comme commencement la hateur du personnage
                    left: '0px',
                })
            };
            break;
        case 37: // va à gauche
            if (deplacementADroiteOuGauche < 1430) {

                $('.bordGauche').css({

                    width: deplacementADroiteOuGauche - 160 + 'px',
                    height: '850px',
                    top: '0px',
                    left: '0px',
                });
                $('.bordDroite').css({

                    width: '1500px',
                    height: '850px',
                    top: '0px',
                    left: deplacementADroiteOuGauche + 'px',
                })
                $('.bordHaut').css({

                    width: '1430px',
                    height: deplacementEnHautOuBas - 40 + 'px',
                    top: '0px',
                    left: '0px',
                })
                $('.bordBas').css({

                    width: '1430px', // largeur de la div
                    height: '850px', // aerty est une variable qui commence à 54. Elle baissera de 4 à chaque fois que la touche bas est appuyée
                    top: deplacementEnHautOuBas + 80 + 'px', // la div aura comme commencement la hateur du personnage
                    left: '0px',
                })
            }
            break;
        case 40: // va en bas
            if (deplacementEnHautOuBas > 0) { //la div qui part du haut et qui diminue quand on monte

                $('.bordBas').css({

                    width: '1430px',
                    height: '850px',
                    top: deplacementEnHautOuBas + 185 + 'px',
                    left: '0px',
                })

                $('.bordHaut').css({

                    width: '1430px',
                    height: deplacementEnHautOuBas + 30 + 'px',
                    top: '0px',
                    left: '0px',
                })
                $('.bordGauche').css({

                    width: deplacementADroiteOuGauche - 50 + 'px',
                    height: '850px',
                    top: '0px',
                    left: '0px',
                })
                $('.bordDroite').css({

                    width: '1500px',
                    height: '850px',
                    top: '0px',
                    left: deplacementADroiteOuGauche + 70 + 'px',
                })
            };
            break;
        case 38: // va en haut
            if (deplacementEnHautOuBas < 900) {

                $('.bordBas').css({

                    width: '1430px',
                    height: '850px',
                    top: deplacementEnHautOuBas + 10 + 'px',
                    left: '0px',
                })

                $('.bordHaut').css({

                    width: '1430px',
                    height: deplacementEnHautOuBas - 150 + 'px',
                    top: '0px',
                    left: '0px',
                })

                $('.bordGauche').css({

                    width: deplacementADroiteOuGauche - 50 + 'px',
                    height: '850px',
                    top: '0px',
                    left: '0px',
                })
                $('.bordDroite').css({

                    width: '1500px',
                    height: '850px',
                    top: '0px',
                    left: deplacementADroiteOuGauche + 70 + 'px',
                })
            };

            break;
        }
    });

    //**********************************************************************************************//
    //***********************************Les fantomes********************************************//
    //**********************************************************************************************//


    $('.espaceJeu').prepend('<div class="fantome1"></div>, <div class="fantome2"></div>, <div class="fantome3"></div>, <div class="fantome4"></div>, <div class="fantome5"></div>, <div class="fantome6"></div>, <div class="fantome7"></div>, <div class="fantome8"></div>, <div class="fantome9"></div>, <div class="fantome10"></div>, <div class="fantome11"></div>');
    $('.fantome1, .fantome2, .fantome3, .fantome4, .fantome5, .fantome6, .fantome7, .fantome8, .fantome9, .fantome10, .fantome11').css({
        width: '30px',
        height: '50px',
        left: '50px',
        top: '100px',
        position: 'absolute',
        zIndex: '1',
    });
    var limite1 = 0;
    var fantome1DroiteGauche = 50; //variable qui sert pour la collision
    var fantome1HautBas = 100;

    setInterval(function () {
        if (limite1 == 0) {
            fantome1HautBas += 5;
            $('.fantome1').css({
                top: fantome1HautBas + 'px',
                backgroundImage: 'url(sprite/sadakobas.png)',
                animation: 'play 1s steps(4) infinite',
            })
        }
        if (limite1 == 1) {
            fantome1HautBas -= 5;
            $('.fantome1').css({
                top: fantome1HautBas + 'px',
                backgroundImage: 'url(sprite/sadakohaut.png)',
                animation: 'play 1s steps(4) infinite',
            })
        }
        if (fantome1HautBas == 300) {
            limite1 = 1;
        }
        if (fantome1HautBas == 100) {
            limite1 = 0;
        }
    }, 50);



    var limite2 = 0;
    var fantome2DroiteGauche = 300;
    var fantome2HautBas = 100; //variable qui sert pour la collision

    setInterval(function () {
        if (limite2 == 0) {
            fantome2DroiteGauche += 5;
            $('.fantome2').css({
                left: fantome2DroiteGauche + 'px',
                backgroundImage: 'url(sprite/sadakoquivagauche.png)',
                animation: 'play 1s steps(4) infinite',
            })
        }
        if (limite2 == 1) {
            fantome2DroiteGauche -= 5;
            $('.fantome2').css({
                left: fantome2DroiteGauche + 'px',
                backgroundImage: 'url(sprite/sadakoquivadroite.png)',
                animation: 'play 1s steps(4) infinite',
            })
        }
        if (fantome2DroiteGauche == 600) {
            limite2 = 1;
        }
        if (fantome2DroiteGauche == 300) {
            limite2 = 0;
        }
    }, 50);

    //troisième fantome


    var limite3 = 0;
    var fantome3DroiteGauche = 400;
    var fantome3HautBas = 400; //variable qui sert pour la collision

    setInterval(function () {
        if (limite3 == 0) {
            fantome3DroiteGauche += 5;
            $('.fantome3').css({
                left: fantome3DroiteGauche + 'px',
                backgroundImage: 'url(sprite/sadakoquivagauche.png)',
                animation: 'play 1s steps(4) infinite',
                top: '390px',
            })
        }
        if (limite3 == 1) {
            fantome3DroiteGauche -= 5;
            $('.fantome3').css({
                left: fantome3DroiteGauche + 'px',
                backgroundImage: 'url(sprite/sadakoquivadroite.png)',
                animation: 'play 1s steps(4) infinite',
                top: '390px',
            })
        }
        if (fantome3DroiteGauche == 670) {
            limite3 = 1;
        }
        if (fantome3DroiteGauche == 400) {
            limite3 = 0;
        }
    }, 50);

    //quatrième fantome


    var limite4 = 0;
    var fantome4DroiteGauche = 100; //variable qui sert pour la collision
    var fantome4HautBas = 400;

    setInterval(function () {
        if (limite4 == 0) {
            fantome4HautBas += 5;
            $('.fantome4').css({
                top: fantome4HautBas + 'px',
                backgroundImage: 'url(sprite/sadakobas.png)',
                animation: 'play 1s steps(4) infinite',
                left: '100px',
            })
        }
        if (limite4 == 1) {
            fantome4HautBas -= 5;
            $('.fantome4').css({
                top: fantome4HautBas + 'px',
                backgroundImage: 'url(sprite/sadakohaut.png)',
                animation: 'play 1s steps(4) infinite',
                left: '100px',
            })
        }
        if (fantome4HautBas == 600) {
            limite4 = 1;
        }
        if (fantome4HautBas == 400) {
            limite4 = 0;
        }
    }, 50);

    //cinquième fantome


    var limite5 = 0;
    var fantome5DroiteGauche = 300;
    var fantome5HautBas = 300; //variable qui sert pour la collision

    setInterval(function () {
        if (limite5 == 0) {
            fantome5DroiteGauche += 5;
            $('.fantome5').css({
                left: fantome5DroiteGauche + 'px',
                backgroundImage: 'url(sprite/sadakoquivagauche.png)',
                animation: 'play 1s steps(4) infinite',
                top: '300px'
            })
        }
        if (limite5 == 1) {
            fantome5HautBas -= 5;
            $('.fantome5').css({
                top: fantome5HautBas + 'px',
                backgroundImage: 'url(sprite/sadakohaut.png)',
                animation: 'play 1s steps(4) infinite',
            })
        }
        if (limite5 == 2) {
            fantome5DroiteGauche -= 5;
            $('.fantome5').css({
                left: fantome5DroiteGauche + 'px',
                backgroundImage: 'url(sprite/sadakoquivadroite.png)',
                animation: 'play 1s steps(4) infinite',
            })
        }
        if (limite5 == 3) {
            fantome5HautBas += 5;
            $('.fantome5').css({
                top: fantome5HautBas + 'px',
                backgroundImage: 'url(sprite/sadakobas.png)',
                animation: 'play 1s steps(4) infinite',
            })
        }
        if (fantome5DroiteGauche == 350 && fantome5HautBas == 300) {
            limite5 = 1;
        }
        if (fantome5DroiteGauche == 300 && fantome5HautBas == 300) {
            limite5 = 0;
        }
        if (fantome5HautBas == 250 && fantome5DroiteGauche == 350) {
            limite5 = 2;
        }
        if (fantome5HautBas == 250 && fantome5DroiteGauche == 300) {
            limite5 = 3;
        }
    }, 50);

    //sixième fantome


    //EXCLU

    //septieme fantome


    //EXCLU

    //huitieme fantome


    //EXCLU

    //neuvième fantome


    //ECLU

    //dixieme fantome


    var limite10 = 0;
    var fantome10DroiteGauche = 800; //variable qui sert pour la collision
    var fantome10HautBas = 100;

    setInterval(function () {
        if (limite10 == 0) {
            fantome10HautBas += 5;
            $('.fantome10').css({
                top: fantome10HautBas + 'px',
                backgroundImage: 'url(sprite/sadakobas.png)',
                animation: 'play 1s steps(4) infinite',
                left: '800px',
            })
        }
        if (limite10 == 1) {
            fantome10HautBas -= 5;
            $('.fantome10').css({
                top: fantome10HautBas + 'px',
                backgroundImage: 'url(sprite/sadakohaut.png)',
                animation: 'play 1s steps(4) infinite',
                left: '800px',
            })
        }
        if (fantome10HautBas == 500) {
            limite10 = 1;
        }
        if (fantome10HautBas == 100) {
            limite10 = 0;
        }
    }, 50);

    // onzième fantome


    //EXCLU
    //**************************************************************************************//
    //*************************** collision des fantomes ************************************//
    //**************************************************************************************//

    setInterval(function () {
        if (deplacementADroiteOuGauche > fantome2DroiteGauche - 21 && deplacementADroiteOuGauche < fantome2DroiteGauche + 21 && deplacementEnHautOuBas > fantome2HautBas - 21 && deplacementEnHautOuBas < fantome2HautBas + 21 ||
            deplacementADroiteOuGauche > fantome1DroiteGauche - 21 && deplacementADroiteOuGauche < fantome1DroiteGauche + 21 && deplacementEnHautOuBas > fantome1HautBas - 21 && deplacementEnHautOuBas < fantome1HautBas + 21 ||
            deplacementADroiteOuGauche > fantome3DroiteGauche - 21 && deplacementADroiteOuGauche < fantome3DroiteGauche + 21 && deplacementEnHautOuBas > fantome3HautBas - 21 && deplacementEnHautOuBas < fantome3HautBas + 21 ||
            deplacementADroiteOuGauche > fantome4DroiteGauche - 21 && deplacementADroiteOuGauche < fantome4DroiteGauche + 21 && deplacementEnHautOuBas > fantome4HautBas - 21 && deplacementEnHautOuBas < fantome4HautBas + 21 ||
            deplacementADroiteOuGauche > fantome5DroiteGauche - 21 && deplacementADroiteOuGauche < fantome5DroiteGauche + 21 && deplacementEnHautOuBas > fantome5HautBas - 21 && deplacementEnHautOuBas < fantome5HautBas + 21 ||
            deplacementADroiteOuGauche > fantome10DroiteGauche - 21 && deplacementADroiteOuGauche < fantome10DroiteGauche + 21 && deplacementEnHautOuBas > fantome10HautBas - 21 && deplacementEnHautOuBas < fantome10HautBas + 21) {
            if (directionCollision == 0) {
                $('.perso').css({
                    backgroundImage: 'url(sprite/persogaucherouge.png)',
                });
            }
            if (directionCollision == 1) {
                $('.perso').css({
                    backgroundImage: 'url(sprite/persodroiterouge.png)',
                });
            }
            if (directionCollision == 2) {
                $('.perso').css({
                    backgroundImage: 'url(sprite/persobasrouge.png)',
                });
            }
            if (directionCollision == 3) {
                $('.perso').css({
                    backgroundImage: 'url(sprite/persohautrouge.png)',
                });
            }
            $('.espaceJeu').prepend('<audio class="sonHit"src="hit.mp3" autoplay></audio>');
            barreDeVie -= 5;
            //alert(barreDeVie);
        }
    }, 200);


    //**************************************************************************************//
    //************************************* compétences ************************************//
    //**************************************************************************************//


    $('.espaceJeu').prepend('<img class="imgHtml" src="html.png" />');
    $('.imgHtml').css({
        height: '30px',
        position: 'absolute',
        top: '450px', //750
        left: '400px',
        zIndex: '1',
    });
    $('.espaceJeu').prepend('<img class="imgCss" src="css.png" />');
    $('.imgCss').css({
        height: '30px',
        position: 'absolute',
        top: '200px',
        left: '400px',
        zIndex: '1',
    });
    $('.espaceJeu').prepend('<img class="imgJs" src="js.png" />');
    $('.imgJs').css({
        height: '30px',
        position: 'absolute',
        top: '400px',
        left: '800px',
        zIndex: '1',
    });
    $('.espaceJeu').prepend('<img class="imgJquery" src="jquery.png" />');
    $('.imgJquery').css({
        height: '30px',
        position: 'absolute',
        top: '200px',
        left: '900px',
        zIndex: '1',
    });
    $('.espaceJeu').prepend('<img class="imgBootstrap" src="bootstrap.png" />');
    $('.imgBootstrap').css({
        height: '30px',
        position: 'absolute',
        top: '500px',
        left: '200px',
        zIndex: '1',
    });
    $('.espaceJeu').prepend('<img class="imgAngular" src="angular.png" />');
    $('.imgAngular').css({
        height: '30px',
        position: 'absolute',
        top: '400px',
        left: '50px',
        zIndex: '1',
    });
    $('.espaceJeu').prepend('<img class="imgPhotoshop" src="photoshop.png" />');
    $('.imgPhotoshop').css({
        height: '30px',
        position: 'absolute',
        top: '400px',
        left: '500px',
        zIndex: '1',
    });
    $('.espaceJeu').prepend('<img class="imgIndesign" src="indesign.png" />');
    $('.imgIndesign').css({
        height: '30px',
        position: 'absolute',
        top: '100px',
        left: '800px',
        zIndex: '1',
    });
    $('.espaceJeu').prepend('<img class="imgNodejs" src="nodejs.png" />');
    $('.imgNodejs').css({
        height: '30px',
        position: 'absolute',
        top: '500px', //800
        left: '900px', //1200
        zIndex: '1',
    });
    $('.espaceJeu').prepend('<img class="imgAjax" src="ajax.png" />');
    $('.imgAjax').css({
        height: '30px',
        position: 'absolute',
        top: '300px',
        left: '300px', //1200
        zIndex: '1',
    });
    $('.espaceJeu').prepend('<img class="imgMongo" src="mongo.png" />');
    $('.imgMongo').css({
        height: '30px',
        position: 'absolute',
        top: '100px',
        left: '300px', //1200
        zIndex: '1',
    });
    $('.espaceJeu').prepend('<img class="imgMeteor" src="meteor.png" />');
    $('.imgMeteor').css({
        height: '30px',
        position: 'absolute',
        top: '100px',
        left: '50px',
        zIndex: '1',
    });


    //*******************************************************************************************//
    //***************************** Aperçu des compétences en haut ******************************//
    //*******************************************************************************************//


    setInterval(function () {

        $('.imgHtml2').remove();
        $('.espaceJeu').prepend('<img class="imgHtml2" src="html2.png" />');
        $('.imgHtml2').css({
            height: '38px',
            position: 'absolute',
            top: '50px',
            left: '253px',
            zIndex: '4',
        });
        $('.imgCss2').remove();
        $('.espaceJeu').prepend('<img class="imgCss2" src="css2.png" />');
        $('.imgCss2').css({
            height: '38px',
            position: 'absolute',
            top: '50px',
            left: '292px',
            zIndex: '4',
        });
        $('.imgJs2').remove();
        $('.espaceJeu').prepend('<img class="imgJs2" src="js2.png" />');
        $('.imgJs2').css({
            height: '38px',
            position: 'absolute',
            top: '50px',
            left: '333px',
            zIndex: '4',
        });
        $('.imgJquery2').remove();
        $('.espaceJeu').prepend('<img class="imgJquery2" src="jquery2.png" />');
        $('.imgJquery2').css({
            height: '38px',
            position: 'absolute',
            top: '50px',
            left: '375px',
            zIndex: '4',
        });
        $('.imgBootstrap2').remove();
        $('.espaceJeu').prepend('<img class="imgBootstrap2" src="bootstrap2.png" />');
        $('.imgBootstrap2').css({
            height: '38px',
            position: 'absolute',
            top: '50px',
            left: '416px',
            zIndex: '4',
        });
        $('.imgAngular2').remove();
        $('.espaceJeu').prepend('<img class="imgAngular2" src="angular2.png" />');
        $('.imgAngular2').css({
            height: '38px',
            position: 'absolute',
            top: '50px',
            left: '457px',
            zIndex: '4',
        });
        $('.imgPhotoshop2').remove();
        $('.espaceJeu').prepend('<img class="imgPhotoshop2" src="photoshop2.png" />');
        $('.imgPhotoshop2').css({
            height: '38px',
            position: 'absolute',
            top: '50px',
            left: '499px',
            zIndex: '4',
        });
        $('.imgIndesign2').remove();
        $('.espaceJeu').prepend('<img class="imgIndesign2" src="indesign2.png" />');
        $('.imgIndesign2').css({
            height: '38px',
            position: 'absolute',
            top: '50px',
            left: '540px',
            zIndex: '4',
        });
        $('.imgNodejs2').remove();
        $('.espaceJeu').prepend('<img class="imgNodejs2" src="nodejs2.png" />');
        $('.imgNodejs2').css({
            height: '38px',
            position: 'absolute',
            top: '50px',
            left: '581px',
            zIndex: '4',
        });
        $('.imgAjax2').remove();
        $('.espaceJeu').prepend('<img class="imgAjax2" src="ajax2.png" />');
        $('.imgAjax2').css({
            height: '38px',
            position: 'absolute',
            top: '50px',
            left: '623px',
            zIndex: '4',
        });
        $('.imgMongo2').remove();
        $('.espaceJeu').prepend('<img class="imgMongo2" src="mongo2.png" />');
        $('.imgMongo2').css({
            height: '38px',
            position: 'absolute',
            top: '50px',
            left: '664px',
            zIndex: '4',
        });
        $('.imgMeteor2').remove();
        $('.espaceJeu').prepend('<img class="imgMeteor2" src="meteor2.png" />');
        $('.imgMeteor2').css({
            height: '38px',
            position: 'absolute',
            top: '50px',
            left: '705px',
            zIndex: '4',
        })

    }, 100);

    //***********************************************************************************//
    //**********collision avec les compétences, disparition des compétences et apparition des compétences sur le haut/bas*******************************************************************************//


    setInterval(function () {


        if (deplacementADroiteOuGauche > 385 && deplacementADroiteOuGauche < 427 && deplacementEnHautOuBas < 479 && deplacementEnHautOuBas > 430) {
            $('.imgHtml').remove();
            $('.espaceJeu').prepend('<img class="imgHtml" src="html.png" />');
            $('.imgHtml').css({
                height: '38px',
                position: 'absolute',
                top: '50px',
                left: '253px',
                zIndex: '4',
            })
            document.getElementById('sonCompetenceHtml').play();
            setTimeout(function () {
                $('#sonCompetenceHtml').remove();
            }, 500);
            tableau[0] = 1;
        }
        if (deplacementADroiteOuGauche > 385 && deplacementADroiteOuGauche < 427 && deplacementEnHautOuBas < 233 && deplacementEnHautOuBas > 177) {
            $('.imgCss').remove();
            $('.espaceJeu').prepend('<img class="imgCss" src="css.png" />');
            $('.imgCss').css({
                height: '38px',
                position: 'absolute',
                top: '50px',
                left: '292px',
                zIndex: '4',
            })
            document.getElementById('sonCompetenceCss').play();
            setTimeout(function () {
                $('#sonCompetenceCss').remove();
            }, 500);
            tableau[1] = 1;
        }
        if (deplacementADroiteOuGauche > 780 && deplacementADroiteOuGauche < 825 && deplacementEnHautOuBas < 429 && deplacementEnHautOuBas > 380) { //1000*400
            $('.imgJs').remove();
            $('.espaceJeu').prepend('<img class="imgJs" src="js.png" />');
            $('.imgJs').css({
                height: '38px',
                position: 'absolute',
                top: '50px',
                left: '333px',
                zIndex: '4',
            })
            document.getElementById('sonCompetenceJavascript').play();
            setTimeout(function () {
                $('#sonCompetenceJavascript').remove();
            }, 500);
            tableau[2] = 1;
        }
        if (deplacementADroiteOuGauche > 880 && deplacementADroiteOuGauche < 929 && deplacementEnHautOuBas < 230 && deplacementEnHautOuBas > 184) { //100*200
            $('.imgJquery').remove();
            $('.espaceJeu').prepend('<img class="imgJquery" src="jquery.png" />');
            $('.imgJquery').css({
                height: '38px',
                position: 'absolute',
                top: '50px',
                left: '375px',
                zIndex: '4',
            })
            document.getElementById('sonCompetenceJquery').play();
            setTimeout(function () {
                $('#sonCompetenceJquery').remove();
            }, 500);
            tableau[3] = 1;
        }
        if (deplacementADroiteOuGauche > 180 && deplacementADroiteOuGauche < 230 && deplacementEnHautOuBas < 530 && deplacementEnHautOuBas > 480) { //200*600
            $('.imgBootstrap').remove();
            $('.espaceJeu').prepend('<img class="imgBootstrap" src="bootstrap.png" />');
            $('.imgBootstrap').css({
                height: '38px',
                position: 'absolute',
                top: '50px',
                left: '416px',
                zIndex: '4',
            })
            document.getElementById('sonCompetenceBootstrap').play();
            setTimeout(function () {
                $('#sonCompetenceBootstrap').remove();
            }, 500);
            tableau[4] = 1;
        }
        if (deplacementADroiteOuGauche > 30 && deplacementADroiteOuGauche < 80 && deplacementEnHautOuBas < 430 && deplacementEnHautOuBas > 380) { //50*400
            $('.imgAngular').remove();
            $('.espaceJeu').prepend('<img class="imgAngular" src="angular.png" />');
            $('.imgAngular').css({
                height: '38px',
                position: 'absolute',
                top: '50px',
                left: '457px',
                zIndex: '4',
            })
            document.getElementById('sonCompetenceAngular').play();
            setTimeout(function () {
                $('#sonCompetenceAngular').remove();
            }, 500);
            tableau[5] = 1;
        }
        if (deplacementADroiteOuGauche > 480 && deplacementADroiteOuGauche < 530 && deplacementEnHautOuBas < 430 && deplacementEnHautOuBas > 380) { //500*400
            $('.imgPhotoshop').remove();
            $('.espaceJeu').prepend('<img class="imgPhotoshop" src="photoshop.png" />');
            $('.imgPhotoshop').css({
                height: '38px',
                position: 'absolute',
                top: '50px',
                left: '499px',
                zIndex: '4',
            })
            document.getElementById('sonCompetencePhotoshop').play();
            setTimeout(function () {
                $('#sonCompetencePhotoshop').remove();
            }, 500);
            tableau[6] = 1;
        }
        if (deplacementADroiteOuGauche > 780 && deplacementADroiteOuGauche < 830 && deplacementEnHautOuBas > 90 && deplacementEnHautOuBas < 130) { //800*30
            $('.imgIndesign').remove();
            $('.espaceJeu').prepend('<img class="imgIndesign" src="indesign.png" />');
            $('.imgIndesign').css({
                height: '38px',
                position: 'absolute',
                top: '50px',
                left: '540px',
                zIndex: '4',
            })
            document.getElementById('sonCompetenceIndesign').play();
            setTimeout(function () {
                $('#sonCompetenceIndesign').remove();
            }, 500);
            tableau[7] = 1;
        }
        if (deplacementADroiteOuGauche > 880 && deplacementADroiteOuGauche < 930 && deplacementEnHautOuBas < 530 && deplacementEnHautOuBas > 480) { //1200*800
            $('.imgNodejs').remove();
            $('.espaceJeu').prepend('<img class="imgNodejs" src="nodejs.png" />');
            $('.imgNodejs').css({
                height: '38px',
                position: 'absolute',
                top: '50px',
                left: '581px',
                zIndex: '4',
            })
            document.getElementById('sonCompetenceNode').play();
            setTimeout(function () {
                $('#sonCompetenceNode').remove();
            }, 500);
            tableau[8] = 1;
        }
        if (deplacementADroiteOuGauche > 280 && deplacementADroiteOuGauche < 330 && deplacementEnHautOuBas < 330 && deplacementEnHautOuBas > 280) { //1200*300
            $('.imgAjax').remove();
            $('.espaceJeu').prepend('<img class="imgAjax" src="ajax.png" />');
            $('.imgAjax').css({
                height: '38px',
                position: 'absolute',
                top: '50px',
                left: '623px',
                zIndex: '4',
            })
            document.getElementById('sonCompetenceAjax').play();
            setTimeout(function () {
                $('#sonCompetenceAjax').remove();
            }, 500);
            tableau[9] = 1;
        }
        if (deplacementADroiteOuGauche > 280 && deplacementADroiteOuGauche < 330 && deplacementEnHautOuBas < 130 && deplacementEnHautOuBas > 90) { //1200*50
            $('.imgMongo').remove();
            $('.espaceJeu').prepend('<img class="imgMongo" src="mongo.png" />');
            $('.imgMongo').css({
                height: '38px',
                position: 'absolute',
                top: '50px',
                left: '664px',
                zIndex: '4',
            })
            document.getElementById('sonCompetenceMongo').play();
            setTimeout(function () {
                $('#sonCompetenceMongo').remove();
            }, 500);
            tableau[10] = 1;
        }
        if (deplacementADroiteOuGauche > 30 && deplacementADroiteOuGauche < 80 && deplacementEnHautOuBas < 130 && deplacementEnHautOuBas > 90) { //50*50
            $('.imgMeteor').remove();
            $('.espaceJeu').prepend('<img class="imgMeteor" src="meteor.png" />');
            $('.imgMeteor').css({
                height: '38px',
                position: 'absolute',
                top: '50px',
                left: '705px',
                zIndex: '4',
            })
            document.getElementById('sonCompetenceMeteor').play();
            setTimeout(function () {
                $('#sonCompetenceMeteor').remove();
            }, 500);
            tableau[11] = 1;
        }

    }, 100);

    //**************************************************************************************************************//
    //*******************************************Quand la personne gagne********************************************//
    //*************************************************************************************************************//




    setInterval(function () {
        if (tableau[0] == 1 && tableau[1] == 1 && tableau[2] == 1 && tableau[3] == 1 && tableau[4] == 1 && tableau[5] == 1 && tableau[6] == 1 && tableau[7] == 1 && tableau[8] == 1 && tableau[9] == 1 && tableau[10] == 1 && tableau[11] == 1) {
            tableau[0] = 2;
            document.getElementById('sonWin').play();
            $('.menuHaut').remove();
            $('body').prepend('<div class="cadreGagne"></div>');
            $('.fourreTout').remove();
            $('.espaceJeu').remove();
            $('.cadreGagne').fadeIn(1000);
            $('.cadreGagne').css({
                position: "absolute",
                top: "50px",
                left: '50px',
                width: '1020px',
                height: '0px',
                backgroundImage: 'url(blue.jpg)',
                borderStyle: 'double',
                borderColor: 'white',
            }).animate({
                height: '700px',
            }, 1000);

            $('.cadreGagne').prepend('<div class="gagneTitre"></div>');
            $('.gagneTitre').css({
                width: '50%',
                height: '100px',
                marginLeft: '25%',
                color: 'white',
                textAlign: 'center',
                fontFamily: 'segoe UI',
                fontSize: '100px',
                position: 'absolute',
            }).text('GAGNÉ !');

            $('.cadreGagne').prepend('<div class="telechargerCV"></div>');
            $('.telechargerCV').css({
                width: '50%',
                height: '50px',
                marginTop: '260px',
                marginLeft: '25%',
                color: 'white',
                textAlign: 'center',
                fontFamily: 'segoe UI',
                fontSize: '20px',
                position: 'absolute',
                padding: '9px',
                cursor: 'pointer',
            }).text('Cliquez dans ce cadre pour télécharger mon CV en PDF');



            $(".telechargerCV").click(function () {
                window.open('http://djoher.com/Cv/CV-Jeu-Djoher.pdf');
            });


            $('.cadreGagne').prepend('<div class="gagneTexte"></div>');
            $('.gagneTexte').css({
                width: '90%',
                height: '200px',
                marginTop: '330px',
                marginLeft: '5%',
                color: 'white',
                textAlign: 'center',
                fontFamily: 'segoe UI',
                fontSize: '30px',
                position: 'absolute',
            }).text('Félicitations ! Vous avez maintenant assez d\'éxpérience pour passer une folle nuit seul(e) dans un hôpital psychatrique abandonné.');

            $('.cadreGagne').prepend('<div class="gagneComp"></div>');
            $('.gagneComp').css({
                width: '100%',
                height: '80px',
                marginTop: '150px',
                marginLeft: '0px',
                color: 'white',
                textAlign: 'center',
                fontFamily: 'segoe UI',
                fontSize: '40px',
                position: 'absolute',
            });

            $('.gagneComp').prepend('<img class="gagneHtml" src="html.png" />');
            $('.gagneHtml').css({
                height: '50px',
                width: '45px',
                position: 'absolute',
                top: '0px',
                left: '10px',
            });
            $('.gagneComp').prepend('<p class="comp1">HTML5</p>');
            $('.comp1').css({
                top: '55px',
                left: '10px',
                position: 'absolute',
                color: 'white',
                fontFamily: 'Sitka small',
                fontSize: '16px',

            });

            $('.gagneComp').prepend('<img class="gagneCss" src="css.png" />');
            $('.gagneCss').css({
                height: '50px',
                width: '45px',
                position: 'absolute',
                top: '0px',
                left: '90px',
            });
            $('.gagneComp').prepend('<p class="comp2">CSS3</p>');
            $('.comp2').css({
                top: '55px',
                left: '100px',
                position: 'absolute',
                color: 'white',
                fontFamily: 'Sitka small',
                fontSize: '16px',
            });

            $('.gagneComp').prepend('<img class="gagneJs" src="js.png" />');
            $('.gagneJs').css({
                height: '50px',
                width: '45px',
                position: 'absolute',
                top: '0px',
                left: '180px',
            });
            $('.gagneComp').prepend('<p class="comp3">Javascript</p>');
            $('.comp3').css({
                top: '55px',
                left: '170px',
                position: 'absolute',
                color: 'white',
                fontFamily: 'Sitka small',
                fontSize: '16px',
            });

            $('.gagneComp').prepend('<img class="gagneJquery" src="jquery.png" />');
            $('.gagneJquery').css({
                height: '50px',
                width: '45px',
                position: 'absolute',
                top: '0px',
                left: '270px',
            });
            $('.gagneComp').prepend('<p class="comp4">Jquery</p>');
            $('.comp4').css({
                top: '55px',
                left: '270px',
                position: 'absolute',
                color: 'white',
                fontFamily: 'Sitka small',
                fontSize: '16px',
            });

            $('.gagneComp').prepend('<img class="gagneBootstrap" src="bootstrap.png" />');
            $('.gagneBootstrap').css({
                height: '50px',
                width: '45px',
                position: 'absolute',
                top: '0px',
                left: '360px',
            });
            $('.gagneComp').prepend('<p class="comp5">Bootstrap</p>');
            $('.comp5').css({
                top: '55px',
                left: '350px',
                position: 'absolute',
                color: 'white',
                fontFamily: 'Sitka small',
                fontSize: '16px',
            });

            $('.gagneComp').prepend('<img class="gagneAngular" src="angular.png" />');
            $('.gagneAngular').css({
                height: '50px',
                width: '45px',
                position: 'absolute',
                top: '0px',
                left: '450px',
            });
            $('.gagneComp').prepend('<p class="comp6">Angular</p>');
            $('.comp6').css({
                top: '55px',
                left: '450px',
                position: 'absolute',
                color: 'white',
                fontFamily: 'Sitka small',
                fontSize: '16px',
            });

            $('.gagneComp').prepend('<img class="gagnePhotoshop" src="photoshop.png" />');
            $('.gagnePhotoshop').css({
                height: '50px',
                width: '45px',
                position: 'absolute',
                top: '0px',
                left: '540px',
            });
            $('.gagneComp').prepend('<p class="comp7">Photoshop</p>');
            $('.comp7').css({
                top: '55px',
                left: '530px',
                position: 'absolute',
                color: 'white',
                fontFamily: 'Sitka small',
                fontSize: '16px',
            });

            $('.gagneComp').prepend('<img class="gagneIndesign" src="indesign.png" />');
            $('.gagneIndesign').css({
                height: '50px',
                width: '45px',
                position: 'absolute',
                top: '0px',
                left: '630px',
            });
            $('.gagneComp').prepend('<p class="comp8">Indesign</p>');
            $('.comp8').css({
                top: '55px',
                left: '620px',
                position: 'absolute',
                color: 'white',
                fontFamily: 'Sitka small',
                fontSize: '16px',
            });

            $('.gagneComp').prepend('<img class="gagneNode" src="nodejs.png" />');
            $('.gagneNode').css({
                height: '50px',
                width: '45px',
                position: 'absolute',
                top: '0px',
                left: '720px',
            });
            $('.gagneComp').prepend('<p class="comp9">NodeJS</p>');
            $('.comp9').css({
                top: '55px',
                left: '720px',
                position: 'absolute',
                color: 'white',
                fontFamily: 'Sitka small',
                fontSize: '16px',
            });

            $('.gagneComp').prepend('<img class="gagneAjax" src="ajax.png" />');
            $('.gagneAjax').css({
                height: '50px',
                width: '45px',
                position: 'absolute',
                top: '0px',
                left: '800px',
            });
            $('.gagneComp').prepend('<p class="comp10">Ajax</p>');
            $('.comp10').css({
                top: '55px',
                left: '810px',
                position: 'absolute',
                color: 'white',
                fontFamily: 'Sitka small',
                fontSize: '16px',
            });

            $('.gagneComp').prepend('<img class="gagneMongo" src="mongo.png" />');
            $('.gagneMongo').css({
                height: '50px',
                width: '45px',
                position: 'absolute',
                top: '0px',
                left: '880px',
            });
            $('.gagneComp').prepend('<p class="comp11">MongoDB</p>');
            $('.comp11').css({
                top: '55px',
                left: '860px',
                position: 'absolute',
                color: 'white',
                fontFamily: 'Sitka small',
                fontSize: '16px',
            });

            $('.gagneComp').prepend('<img class="gagneMeteor" src="meteor.png" />');
            $('.gagneMeteor').css({
                height: '50px',
                width: '45px',
                position: 'absolute',
                top: '0px',
                left: '960px',
            });
            $('.gagneComp').prepend('<p class="comp12">Meteor</p>');
            $('.comp12').css({
                top: '55px',
                left: '950px',
                position: 'absolute',
                color: 'white',
                fontFamily: 'Sitka small',
                fontSize: '16px',
            });


            $('.cadreGagne').prepend('<div class="entreprise"></div>');
            $('.entreprise').css({
                width: '80%',
                height: '50px',
                marginTop: '470px',
                marginLeft: '10%',
                color: 'gold',
                textAlign: 'center',
                fontFamily: 'segoe UI',
                fontSize: '35px',
                position: 'absolute',
            }).text('Vous me voulez dans votre société pour un stage de 4 mois ou un futur emploi ? Contactez moi.');

            $('.cadreGagne').prepend('<div class="mail"></div>');
            $('.mail').css({
                width: '20%',
                height: '50px',
                marginTop: '610px',
                marginLeft: '10%',
                color: 'white',
                textAlign: 'center',
                fontFamily: 'segoe UI',
                fontSize: '30px',
                position: 'absolute',
            }).text('khaled@djoher.com');

            $('.cadreGagne').prepend('<div class="telephone"></div>');
            $('.telephone').css({
                width: '20%',
                height: '50px',
                marginTop: '610px',
                marginLeft: '40%',
                color: 'white',
                textAlign: 'center',
                fontFamily: 'segoe UI',
                fontSize: '30px',
                position: 'absolute',
            }).text('06 46 91 11 48');


            $('.cadreGagne').prepend('<div class="facebook"></div>');
            $('.facebook').css({
                width: '25%',
                height: '50px',
                marginTop: '590px',
                marginLeft: '70%',
                color: 'white',
                textAlign: 'center',
                fontFamily: 'segoe UI',
                fontSize: '26px',
                position: 'absolute',
                cursor: 'pointer',
            }).text('Cliquez pour me contacter via LinkedIn');

            $(".facebook").click(function () {
                window.open('https://fr.linkedin.com/in/djoher-khaled-b51388b0');
            });
        }
    }, 1000);



    //********************************************************************************************//
    //**************************Menus du haut******************************************************//
    //********************************************************************************************//


    $('body').prepend('<div class="menuHaut"></div>');
    $('.menuHaut').css({
        width: '100%',
        height: '40px',
        position: 'absolute',
        zIndex: '5',
        top: '710px',
    })

    $('.menuHaut').prepend('<div class="menuContacter"></div>');
    $('.menuContacter').css({
        width: '20%',
        height: '30px',
        marginTop: '5px',
        marginLeft: '5%',
        color: 'gold',
        textAlign: 'center',
        fontFamily: 'segoe UI',
        fontSize: '20px',
        position: 'absolute',
    }).text('Me contacter par mail');
    $('.menuContacter').hover(function () {
        $(this).css({}).text('khaled@djoher.com');
    }, function () {
        $(this).css({}).text('Me contacter par mail');
    });

    $('.menuHaut').prepend('<div class="menuCv"></div>');
    $('.menuCv').css({
        width: '20%',
        height: '30px',
        marginTop: '5px',
        marginLeft: '45%',
        color: 'gold',
        textAlign: 'center',
        fontFamily: 'segoe UI',
        fontSize: '5px',
        position: 'absolute',
        fontSize: '20px',
        cursor: 'pointer',
    }).text('Télécharger mon CV');
    $(".menuCv").click(function () {
        window.open('http://djoher.com/Cv/CV-Jeu-Djoher.pdf');
    });

    $('.menuHaut').prepend('<div class="menuFb"></div>');
    $('.menuFb').css({
        width: '20%',
        height: '30px',
        marginTop: '5px',
        marginLeft: '25%',
        color: 'gold',
        textAlign: 'center',
        fontFamily: 'segoe UI',
        fontSize: '5px',
        position: 'absolute',
        fontSize: '20px',
        cursor: 'pointer',
    }).text('Me contacter via LinkedIn');
    $(".menuFb").click(function () {
        window.open('https://fr.linkedin.com/in/djoher-khaled-b51388b0');
    });


    //Message si la fenetre est trop petite.
     if ($(window).width() < 1020 || $(window).height() < 700) {
        $('.espaceJeu').remove();
        $('.menuHaut').remove();
        $('.cadrePresentation').remove();
        $('.cadrePresentationFond').remove();
        $('body').prepend('<div class="erreur"></div>');
        $('.erreur').css({
            width: '100%',
            color: 'black',
            fontSize: '30px',
            textAlign: 'center',
            fontFamily: 'segoe UI',
        }).text('Désolé, le jeu est fait pour des résolutions supérieurs à 1020x700. Si vous êtes sur un ordinateur, vous pouvez toujours augmenter la résolution de votre navigateur en maintenant la touche CTRL et en jouant avec la molette de votre souris. Lorsque c\'est fait, vous pouvez rafraichir la page avec F5.');
        $('body').prepend('<div class="imgErreur"></div>');
        $('.imgErreur').css({
            backgroundImage: 'url(14502.png)',
            width: '60%',
            height: '400px',
            marginLeft: '20%',
            backgroundRepeat: 'no-repeat',
        });
    };

    //chargement

    $('body').prepend('<div class="chargement"></div>');
    $('.chargement').css({
        backgroundColor : 'white',
        width: '100%',
        height: '100%',
        position: 'absolute',
        zIndex: '6',
    })

    $('.chargement').prepend('<img class="gif" src="loading.gif"/>');
    $('.gif').css({
        width: '295px',
        height: '295px',
        top: '200px',
        marginLeft: '30%',
        position : 'absolute',
    })

    setTimeout(function () {
        $('.chargement').remove();
    }, 3000);


});