var deplacementADroiteOuGauche; // variable du deplacement à gauche et à droite
var deplacementEnHautOuBas; // variable du deplacement en haut et en bas
var eclair = 0; // variable pour l'éclair
var directionCollision = 0;
var tableau = [];
var nuage = 0;
var nuageNombre = 0;


$(function () {



    //********************************************************************************************//
    //**************************Menus du haut******************************************************//
    //********************************************************************************************//


    $('body').prepend('<div class="menuHaut"></div>');
    $('.menuHaut').css({
        width: '100%',
        height: '40px',
        position: 'absolute',
        zIndex: '5',
        top: '710px',
    })

    $('.menuHaut').prepend('<div class="menuContacter"></div>');
    $('.menuContacter').css({
        width: '20%',
        height: '30px',
        marginTop: '5px',
        marginLeft: '5%',
        color: 'gold',
        textAlign: 'center',
        fontFamily: 'segoe UI',
        fontSize: '20px',
        position: 'absolute',
    }).text('Me contacter par mail');
    $('.menuContacter').hover(function () {
        $(this).css({}).text('khaled@djoher.com');
    }, function () {
        $(this).css({}).text('Me contacter par mail');
    });

    $('.menuHaut').prepend('<div class="menuCv"></div>');
    $('.menuCv').css({
        width: '20%',
        height: '30px',
        marginTop: '5px',
        marginLeft: '45%',
        color: 'gold',
        textAlign: 'center',
        fontFamily: 'segoe UI',
        fontSize: '5px',
        position: 'absolute',
        fontSize: '20px',
        cursor: 'pointer',
    }).text('Télécharger mon CV');
    $(".menuCv").click(function () {
        window.open('http://djoher.com/Cv/CV-Jeu-Djoher.pdf');
    });

    $('.menuHaut').prepend('<div class="menuFb"></div>');
    $('.menuFb').css({
        width: '20%',
        height: '30px',
        marginTop: '5px',
        marginLeft: '25%',
        color: 'gold',
        textAlign: 'center',
        fontFamily: 'segoe UI',
        fontSize: '5px',
        position: 'absolute',
        fontSize: '20px',
        cursor: 'pointer',
    }).text('Me contacter via LinkedIn');
    $(".menuFb").click(function () {
        window.open('https://fr.linkedin.com/in/djoher-khaled-b51388b0');
    });
    //******************************************************************************************//
    //*****************************Présentation************************************************//
    //******************************************************************************************//

$('body').prepend('<div class="cadrePresentationFond"></div>');
    $('.cadrePresentationFond').css({
       backgroundImage: 'url(blue.jpg)',
        marginLeft: '0%',
        width: '100%',
        height: '100%',
        zIndex: '4',
        position: 'absolute',
    });



    //div principale
    $('body').prepend('<div class="cadrePresentation"></div>');
    $('.cadrePresentation').css({
        marginLeft: '10%',
        width: '80%',
        height: '900px',
        zIndex: '5',
        position: 'absolute',
        marginRight: '20%',
    });

    //div de présentation principale
    $('.cadrePresentation').prepend('<div class="titre"></div>');
    $('.titre').css({
        backgroundColor: 'springgreen',
        marginLeft: '20px',
        top: '0px',
        width: '180px',
        height: '176px',
        fontFamily: 'impact',
        fontSize: '50px',
        color: 'white',
        textAlign: 'center',
        paddingTop : '12px',
    }).text('KHALED DJOHER');
    
    
    $('.cadrePresentation').prepend('<img class ="photo2" src="photo.png"/>');
    $('.photo2').css({
        top :'0px',
        left : '200px',
        position : 'absolute',
    })


    //div pour le texte de présentation
    $('.cadrePresentation').prepend('<div class="texteDePresentation"></div>');
    $('.texteDePresentation').css({
        left: '10px',
        top: '210px',
        width: '700px',
        height: '100px',
        position: 'absolute',
        fontFamily: 'segoe UI',
        fontSize: '17px',
        color: 'white',
        textAlign: 'center',
    }).text('Bonjour à vous. Je suis Khaled Djoher, étudiant Javascript Full Stack (front-end et back-end) à l\'Ifocop. Je recherche actuellement un stage de fin d\'étude ou un futur emploi. Mais au lieu de vous envoyer un PDF de mon CV comme vous en voyez tous les jours, j\'ai décidé de vous montrer un petit aperçu de ce que je sais faire. Êtes-vous prêt à vous balader dans la maison hantée que j\'ai entièrement codée ?');

    //div pour le titre du jeu
    $('.cadrePresentation').prepend('<div class="titreJeu"></div>');
    $('.titreJeu').css({
        backgroundColor: 'limegreen',
        left: '740px',
        top: '180px',
        width: '180px',
        height: '180px',
        position: 'absolute',
        backgroundRepeat: 'no-repeat',
        fontFamily: 'segoe UI , verdana',
        fontSize: '36px',
        color: 'white',
        textAlign: 'center',
        paddingTop : '40px',
        //fontWeight : 'bold',
    }).text('The Silent House');

    //div pour le film
    $('.cadrePresentation').prepend('<div class="film"></div>');
    $('.film').css({
        backgroundColor: 'green',
        left: '920px',
        top: '180px',
        width: '180px',
        height: '180px',
        position: 'absolute',
        fontFamily: 'segoe UI',
        fontSize: '25px',
        color: 'white',
        textAlign: 'center',
        paddingTop : '20px',
        paddingLeft : '10px',
        paddingRight : '10px',
        cursor : 'pointer',
    }).text('Jeu inspiré du film espagnol "La casa Muda"');
    $(".film").click(function () {
        window.open('http://www.allocine.fr/film/fichefilm_gen_cfilm=180607.html');
    });

    $('.cadrePresentation').prepend('<img class="cadre1" src="coder.jpg" />');
    $('.cadre1').css({
        left: '20px',
        top: '360px',
        width: '180px',
        height: '180px',
        position: 'absolute',
    });

    $('.cadrePresentation').prepend('<div class="cadre2"></div>');
    $('.cadre2').css({
        backgroundColor: 'green',
        left: '200px',
        top: '360px',
        width: '180px',
        height: '180px',
        position: 'absolute',
        fontFamily: 'segoe UI',
        fontSize: '25px',
        color: 'white',
        textAlign: 'center',
        paddingTop : '20px',
        paddingLeft : '10px',
        paddingRight : '10px',
    }).text('Se déplacer avec les touches directionnelles');

    $('.cadrePresentation').prepend('<div class="cadre3"></div>');
    $('.cadre3').css({
        backgroundColor: 'limegreen',
        left: '380px',
        top: '360px',
        width: '180px',
        height: '180px',
        position: 'absolute',
        fontFamily: 'Gabriola',
        fontSize: '20px',
        color: 'white',
        textAlign: 'center',
    });

    $('.cadrePresentation').prepend('<img class="cadre4" src="creation.jpg" />');
    $('.cadre4').css({
        left: '560px',
        top: '360px',
        width: '180px',
        height: '180px',
        position: 'absolute',
    });

    $('.cadrePresentation').prepend('<div class="cadre5"></div>');
    $('.cadre5').css({
        backgroundColor: 'green',
        left: '740px',
        top: '360px',
        width: '180px',
        height: '180px',
        position: 'absolute',
        fontFamily: 'segoe UI',
        fontSize: '30px',
        color: 'white',
        textAlign: 'center',
        paddingTop : '20px',
        paddingLeft : '10px',
        paddingRight : '10px',
    }).text('Utiliser la compétence "éclair"');

    $('.cadrePresentation').prepend('<div class="cadre6"></div>');
    $('.cadre6').css({
        backgroundColor: 'limegreen',
        left: '920px',
        top: '360px',
        width: '180px',
        height: '180px',
        position: 'absolute',
        fontFamily: 'Gabriola',
        fontSize: '20px',
        color: 'white',
        textAlign: 'center',
    });

    $('.cadrePresentation').prepend('<div class="cadre7"></div>');
    $('.cadre7').css({
        backgroundColor: 'darkolivegreen',
        left: '740px',
        top: '540px',
        width: '180px',
        height: '160px',
        position: 'absolute',
        fontFamily: 'segoe UI',
        fontSize: '30px',
        color: 'white',
        textAlign: 'center',
         paddingTop : '10px',
        paddingLeft : '10px',
        paddingRight : '10px',
    }).text('Pour commencer, cliquez ici');

    

    //div de la commande 1
    $('.cadre3').prepend('<div class="commande1"></div>');
    $('.commande1').css({
        marginTop: '0px',
        width: '30%',
        height: '170px',
        marginLeft: '0px',
        position: 'absolute',
    })




    //div des touches 
    $('.commande1').prepend('<div class="touches1"></div>');
    $('.touches1').css({
        backgroundImage: 'url(tuto.png)',
        marginTop: '50px',
        width: '48px',
        height: '66px',
        marginLeft: '65px',
        position: 'absolute',
        animation: 'playpersotuto 2s steps(4) infinite',
    })

    $('.commande1').prepend('<div class="toucheHaut"></div>');
    $('.toucheHaut').css({
        backgroundImage: 'url(touchehaut2.png)',
        marginTop: '5px',
        width: '40px',
        height: '40px',
        marginLeft: '70px',
        position: 'absolute',
    })

    $('.commande1').prepend('<div class="toucheGauche"></div>');
    $('.toucheGauche').css({
        backgroundImage: 'url(touchedroite2.png)',
        marginTop: '60px',
        width: '40px',
        height: '40px',
        marginLeft: '20px',
        position: 'absolute',
    })

    $('.commande1').prepend('<div class="toucheBas"></div>');
    $('.toucheBas').css({
        backgroundImage: 'url(touchebas2.png)',
        marginTop: '120px',
        width: '40px',
        height: '40px',
        marginLeft: '70px',
        position: 'absolute',
    })

    $('.commande1').prepend('<div class="toucheDroite"></div>');
    $('.toucheDroite').css({
        backgroundImage: 'url(touchegauche2.png)',
        marginTop: '60px',
        width: '40px',
        height: '40px',
        marginLeft: '120px',
        position: 'absolute',
    })


    //l'aperçu
    $('.cadre6').prepend('<div class="apercuImage"></div>');
    $('.apercuImage').css({
        backgroundImage: 'url(apercuimage.png)',
        marginTop: '20px',
        width: '100px',
        height: '100px',
        marginLeft: '40px',
        position: 'absolute',
        animation: 'apercu 2s steps(2) infinite',
        borderStyle: 'double',
        borderColor: 'white',
    })

    //touche space
    $('.cadre6').prepend('<div class="toucheSpace"></div>');
    $('.toucheSpace').css({
        backgroundImage: 'url(touchespace.png)',
        marginTop: '130px',
        width: '120px',
        height: '30px',
        marginLeft: '30px',
        position: 'absolute',
    })


    //div d'explication

    $('.cadrePresentation').prepend('<div class="explication"></div>');
    $('.explication').css({
        left: '10px',
        top: '570px',
        width: '700px',
        height: '50px',
        position: 'absolute',
        fontFamily: 'segoe UI',
        fontSize: '18px',
        color: 'white',
        textAlign: 'center',
         paddingTop : '10px',
        paddingLeft : '10px',
        paddingRight : '10px',
    }).text('Avec votre lampe torche, fouillez la grande pièce afin de trouver mes compétences en développement web. Mais attention ! Des jeunes filles en robe blanche et aux cheveux longs se baladent dans cet endroit lugubre. Et elles ne sont pas du genre à vous faire la bise...');


    $('.cadre7').hover(function () {
        $(this).css({
            backgroundColor: 'darkgreen',
            cursor: 'pointer',
        });
    }, function () {
        $(this).css({
            backgroundColor: 'darkolivegreen',
        })
    });

    //div qui commence le jeu quand on clique dessus
     $(".cadre7").click(function () {
                window.open('http://djoher.com/jeux', '_self', false);
            });






    //Message si la fenetre est trop petite.
    if ($(window).width() < 1020 || $(window).height() < 700) {
        $('.espaceJeu').remove();
        $('.menuHaut').remove();
        $('.cadrePresentation').remove();
        $('.cadrePresentationFond').remove();
        $('body').prepend('<div class="erreur"></div>');
        $('.erreur').css({
            width: '100%',
            color: 'black',
            fontSize: '30px',
            textAlign: 'center',
            fontFamily: 'segoe UI',
        }).text('Désolé, le jeu est fait pour des résolutions supérieurs à 1020x700. Si vous êtes sur un ordinateur, vous pouvez toujours augmenter la résolution de votre navigateur en maintenant la touche CTRL et en jouant avec la molette de votre souris. Lorsque c\'est fait, vous pouvez rafraichir la page avec F5.');
        $('body').prepend('<div class="imgErreur"></div>');
        $('.imgErreur').css({
            backgroundImage: 'url(14502.png)',
            width: '60%',
            height: '400px',
            marginLeft: '20%',
            backgroundRepeat: 'no-repeat',
        });
    };





});